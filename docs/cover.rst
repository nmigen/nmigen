The nMigen documentation
########################

.. toctree::
   :maxdepth: 1

   index
   Board definitions <https://nmigen.net/nmigen-boards/>
