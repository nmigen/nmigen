.. meta::
    :google-site-verification: jMd4zkKWmq9phPv65ZtCaQeePcI_0xyL_M6v52XlRnw
.. meta::
    :description: nMigen - a Python HDL toolkit for ASIC and FPGA

Language & toolchain
####################

.. warning::

   This manual is a work in progress and is seriously incomplete!

.. toctree::
   :maxdepth: 2

   intro
   install
   start
   tutorial
   lang
   stdlib
   platform
   changes
